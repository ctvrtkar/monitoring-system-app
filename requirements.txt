asgiref==3.5.0
Django==4.0.4
sqlparse==0.4.2
pyserial==3.5
django-crontab==0.7.1
influxdb_client==1.28.0
requests==2.27.1