from struct import *
import serial
from django.conf import settings
from .models import Module, ChannelDescriptor
import random

master_addr = bytearray(0x0001.to_bytes(2, "little"))


class DevDescriptor:
    data_types = None

    def __init__(self, type_id, n_channels, data_desc_len, sn, fw_ver):
        self.type_id = type_id
        self.n_channels = n_channels
        self.data_desc_len = data_desc_len
        self.sn = sn
        self.fw_ver = fw_ver


class RS485:
    _ser: serial

    def __init__(self):
        if settings.RS485_DEBUG:
            return
        self._ser = serial.Serial(settings.RS485_SERIAL_PORT,
                                  baudrate=115200,
                                  parity=serial.PARITY_NONE,
                                  stopbits=serial.STOPBITS_ONE,
                                  timeout=2)

    def __del__(self):
        if settings.RS485_DEBUG:
            return
        self._ser.close()

    def send(self, addr: int, data: bytearray):  # input bytearray
        tx_data: bytearray = data
        tx_data[0:0] = bytearray(addr.to_bytes(2, "little"))
        self._ser.write(tx_data)

    def recv(self, data_len):
        recv_data = self._ser.read(data_len + 2)
        if not recv_data:
            raise SystemExit
        if len(recv_data) > 0:
            if recv_data[0:2] == master_addr:
                return recv_data[2:len(recv_data)]

    def get_descriptor(self, addr: int):
        if settings.RS485_DEBUG:
            return DevDescriptor(
                type_id=0x22,
                n_channels=2,
                fw_ver=8754,
                sn=784,
                data_desc_len=2
            )
        self.send(addr, bytearray(0xa0.to_bytes(1, "little")))
        bin_descriptor = self.recv(12)
        dec_des = unpack("=BBHII", bin_descriptor)
        return DevDescriptor(dec_des[0], dec_des[1], dec_des[2], dec_des[3], dec_des[4])

    def get_data_descriptor(self, addr: int, n_val):
        if settings.RS485_DEBUG:
            return bytearray([0x01, 0x02])
        self.send(addr, bytearray(0xa1.to_bytes(1, "little")))
        bin_datatypes = self.recv(n_val)  # byte per value type (array)

        return bin_datatypes

    def get_measurement(self, dev):
        if settings.RS485_DEBUG:
            return [random.uniform(18, 35), random.uniform(0, 100)]
        channels = ChannelDescriptor.objects.filter(dev_type__module=dev).order_by('ch').all()
        self.send(dev.address, bytearray(0xb0.to_bytes(1, "little")))
        bin_data = self.recv(dev.dev_type.n_channels * 4)
        conversion_string = ""
        for channel in channels:
            conversion_string += channel.value_type.conv_letter
        return unpack("=" + conversion_string, bin_data)
