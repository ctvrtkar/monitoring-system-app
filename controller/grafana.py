import json
import requests
from django.conf import settings
from .models import Module, ChannelDescriptor
from copy import deepcopy

grafana_headers = {
    "Authorization": "Bearer " + settings.GRAFANA_API_TOKEN,
    "Content-Type": "application/json",
    "Accept": "application/json"
}

dashboard_template = {
    "dashboard": {
        "id": None,
        "uid": None,
        "title": "Basic Dashboard",
        "tags": [],
        "timezone": "browser",
        "schemaVersion": 16,
        "version": 0,
        "panels": []
    },
    "folderId": 0,
    "overwrite": False
}

panel_template = {
    "datasource": {
        "type": "influxdb",
        "uid": "PACBEEDECF159CDCA"
    },
    "fieldConfig": {
        "defaults": {
            "color": {
                "mode": "palette-classic"
            },
            "custom": {
                "axisLabel": "Axis Label",  # value unit
                "axisPlacement": "auto",
                "barAlignment": 0,
                "drawStyle": "line",
                "fillOpacity": 0,
                "gradientMode": "none",
                "hideFrom": {
                    "legend": False,
                    "tooltip": False,
                    "viz": False
                },
                "lineInterpolation": "linear",
                "lineWidth": 1,
                "pointSize": 5,
                "scaleDistribution": {
                    "type": "linear"
                },
                "showPoints": "auto",
                "spanNulls": False,
                "stacking": {
                    "group": "A",
                    "mode": "none"
                },
                "thresholdsStyle": {
                    "mode": "off"
                }
            },
            "mappings": [],

        },
        "overrides": []
    },
    "gridPos": {
        "h": 9,
        "w": 12,
        "x": 0,
        "y": 0
    },
    "id": 0,  # custom ID
    "options": {
        "legend": {
            "calcs": [],
            "displayMode": "list",
            "placement": "bottom"
        },
        "tooltip": {
            "mode": "single",
            "sort": "none"
        }
    },
    "targets": [
        {
            "datasource": {
                "type": "influxdb",
                "uid": "PACBEEDECF159CDCA"
            },
            "query": "",
            "refId": "A"
        }
    ],
    "title": "Name",  # panel name
    "type": "timeseries"
}

flux_query_template = "from(bucket: \"{bucket}\")\n  " \
                      "|> range(start: v.timeRangeStart, stop: v.timeRangeStop)\n  " \
                      "|> filter(fn: (r) => r[\"_measurement\"] == \"environment\")\n  " \
                      "|> filter(fn: (r) => r[\"_field\"] == \"{channel}\")\n  " \
                      "|> filter(fn: (r) => r[\"addr\"] == \"{address}\")\n  " \
                      "|> aggregateWindow(every: v.windowPeriod, fn: mean, createEmpty: false)\n  " \
                      "|> yield(name: \"mean\")"


def grafana_get_datasource():
    datasource_url = settings.GRAFANA_URL + "api/datasources/name/" + settings.GRAFANA_DATASOURCE_NAME
    res = requests.get(headers=grafana_headers, url=datasource_url)
    res = res.json()
    return res['uid']


def grafana_add_dashboard(module: Module):
    dashboard_data = deepcopy(dashboard_template)
    dashboard_data['dashboard']['title'] = module.name
    datasource_uid = grafana_get_datasource()

    mod_channels = ChannelDescriptor.objects.filter(dev_type=module.dev_type).order_by('ch').all()
    for channel in mod_channels:
        print("channel")
        print(channel)
        tmp_query = flux_query_template.format(bucket=settings.INFLUXDB_BUCKET,
                                               channel="ch" + str(channel.ch),
                                               address=module.address
                                               )

        tmp_panel = deepcopy(panel_template)
        tmp_panel['datasource']['uid'] = datasource_uid
        tmp_panel['fieldConfig']['defaults']['custom']['axisLabel'] = channel.value_type.name
        tmp_panel['id'] = channel.ch+1
        tmp_panel['targets'][0]['datasource']['uid'] = datasource_uid
        tmp_panel['targets'][0]['query'] = tmp_query
        tmp_panel['title'] = channel.description

        dashboard_data['dashboard']['panels'].append(tmp_panel)

    dashboard_url = settings.GRAFANA_URL + "api/dashboards/db"
    res = requests.post(url=dashboard_url, headers=grafana_headers, data=json.dumps(dashboard_data), verify=False)
    dashboard = {
        'status': "",
        'uid': "",
        'slug': ""
    }
    if res.status_code == 200:
        reply = res.json()
        dashboard['status'] = reply['status']
        dashboard['uid'] = reply['uid']
        dashboard['slug'] = reply['slug']

    else:
        dashboard['status'] = "error"

    return dashboard


def grafana_del_dashboard(module: Module):
    dashboard_url = settings.GRAFANA_URL + "api/dashboards/uid/" + module.dashboard_uid
    res = requests.delete(url=dashboard_url, headers=grafana_headers)
    return res.status_code == 200

