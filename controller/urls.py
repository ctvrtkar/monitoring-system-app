from django.urls import path
from . import views

urlpatterns = [
    path('', views.dashboard),
    path('modules/', views.modules_view),
    path('modules/add', views.add_module),
    path('limit/edit', views.limit_edit)
]
