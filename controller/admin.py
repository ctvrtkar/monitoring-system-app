from django.contrib import admin
from controller.models import *


class ModuleAdmin(admin.ModelAdmin):
    list_display = ('name', 'address', 'dev_type', 'serial_number')
    pass


class ValueTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'unit', 'code')
    pass


class DevTypeDescriptorAdmin(admin.ModelAdmin):
    list_display = ('name', 'code')
    pass


class ChannelDescriptorAdmin(admin.ModelAdmin):
    list_display = ('ch', 'dev_type', 'value_type')
    pass


class ValueLimitsAdmin(admin.ModelAdmin):
    list_display = ('channel', 'min_value', 'max_value')
    pass


class NotifyContactlistAdmin(admin.ModelAdmin):
    list_display = ('name', 'email_address')


admin.site.register(Module, ModuleAdmin)
admin.site.register(ValueType, ValueTypeAdmin)
admin.site.register(ValueLimit, ValueLimitsAdmin)
admin.site.register(DevTypeDescriptor, DevTypeDescriptorAdmin)
admin.site.register(ChannelDescriptor, ChannelDescriptorAdmin)
admin.site.register(NotifyContactlist, NotifyContactlistAdmin)