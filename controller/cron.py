from .models import *
import serial
import influxdb_client
from controller.views import email_notify
from .rs485 import RS485
from django.conf import settings


def hello():
    # stat = Status.objects.create(data_collection_running=True)
    print("hello from cron")


def read_data():

    client = influxdb_client.InfluxDBClient(
        url=settings.INFLUXDB_URL,
        token=settings.INFLUXDB_API_TOKEN,
        org=settings.INFLUXDB_ORG
    )
    write_api = client.write_api()

    bus = RS485()

    modules = Module.objects.filter(status=0).all()
    data = None
    for module in modules:
        failed = False
        for attempt in range(3):
            print(f"Attempt {attempt}")
            try:
                data = bus.get_measurement(module)
                break
            except SystemExit:
                if attempt >= 2:
                    module.status = 2
                    module.save()
                    email_notify("Error", f"Device {module.name} not responding.")
                    failed = True
        if failed:
            continue

        db_line = "{measurement},addr={addr} ".format(measurement="environment",
                                                      addr=module.address)
        channels = ChannelDescriptor.objects.filter(dev_type__module=module).order_by('ch').all()
        first = True
        for channel in channels:
            # v.value_type.name.lower(),
            if first:
                first = False
            else:
                db_line += ","
            db_line += "ch{channel}={val}".format(channel=channel.ch,
                                                  val=data[channel.ch])

            channel_limit = ValueLimit.objects.filter(module=module, channel=channel).first()
            if channel_limit:
                if data[channel.ch] > channel_limit.max_value:
                    print("is higher")
                    email_notify("WARNING", f"Module {module.name} channel {channel.ch} is over max limit")
                elif data[channel.ch] < channel_limit.min_value:
                    print("is lower")
                    email_notify("WARNING", f"Module {module.name} channel {channel.ch} is under min limit")

        print(db_line)
        write_api.write("monitoring", "fourpoint", db_line)
    write_api.close()
