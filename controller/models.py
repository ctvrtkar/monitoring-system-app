from django.db import models


# Create your models here.


class ValueType(models.Model):
    name = models.CharField(max_length=30)
    unit = models.CharField(max_length=10)
    code = models.IntegerField()
    conv_letter = models.CharField(max_length=1)

    def __str__(self):
        return f"{self.name}"


class DevTypeDescriptor(models.Model):
    name = models.CharField(max_length=100, default="Unknown dev type")
    description = models.CharField(max_length=400, null=True, blank=True)
    code = models.IntegerField(unique=True)
    n_channels = models.IntegerField()

    def __str__(self):
        return f"{self.name} ({hex(self.code)})"


class ChannelDescriptor(models.Model):
    ch = models.IntegerField()
    description = models.CharField(max_length=50, default="Unknown channel")
    dev_type = models.ForeignKey(DevTypeDescriptor, on_delete=models.CASCADE)
    value_type = models.ForeignKey(ValueType, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.dev_type} - CH{self.ch} {self.value_type}"  # CHx Value


class Module(models.Model):
    address = models.PositiveIntegerField()
    name = models.CharField(max_length=100)
    dev_type = models.ForeignKey(DevTypeDescriptor, on_delete=models.CASCADE)
    firmware_version = models.PositiveIntegerField()
    serial_number = models.PositiveIntegerField()
    status = models.IntegerField()
    dashboard_uid = models.CharField(max_length=100)
    dashboard_slug = models.CharField(max_length=100)

    def hex_address(self):
        return hex(self.address)

    def __str__(self):
        return f"{self.name} {self.hex_address()}"


class ValueLimit(models.Model):
    channel = models.ForeignKey(ChannelDescriptor, on_delete=models.CASCADE)
    module = models.ForeignKey(Module, on_delete=models.CASCADE)
    min_value = models.FloatField(null=True, blank=True)
    max_value = models.FloatField(null=True, blank=True)

    def __str__(self):
        return f"{self.channel} limit"


class NotifyContactlist(models.Model):
    name = models.CharField(max_length=200)
    email_address = models.EmailField(max_length=100)
