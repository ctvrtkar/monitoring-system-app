from django.shortcuts import render
from django.conf import settings
from .models import *
from .rs485 import RS485
from django.shortcuts import redirect
from django.core.mail import send_mail
from .grafana import grafana_add_dashboard, grafana_del_dashboard


def dashboard(request):
    # email_notify("test", "test")
    modules = Module.objects \
        .all() \
        .prefetch_related('dev_type')

    devices = []

    for module in modules:
        mod = {
            'name': module.name,
            'address': module.address,
            'address_hex': module.hex_address(),
            'type': module.dev_type.name,
            'dash_uid': module.dashboard_uid,
            'dash_slug': module.dashboard_slug,
            'channels': [],
            'status': module.status
        }
        mod_channels = ChannelDescriptor.objects.filter(dev_type__module=module).order_by('ch').all()
        for channel in mod_channels:
            mod['channels'].append(channel.ch + 1)

        devices.append(mod)
    return render(request, 'dashboard.html', {'modules': devices, 'grafana_ulr': settings.GRAFANA_PROXY_URL})


def modules_view(request):
    message = {
        'title': None,
        'text': None,
        'type': None
    }
    if request.method == 'POST':
        addr = int(request.POST['remove-addr'])
        mod_for_deletion = Module.objects.get(address=addr)
        grafana_del_dashboard(mod_for_deletion)
        mod_for_deletion.delete()
        message['title'] = "Module Removed"
        message['text'] = f"Module with address {hex(addr)} was removed"
        message['type'] = "success"

    modules = Module.objects \
        .all() \
        .prefetch_related('dev_type')

    devices = []

    for module in modules:
        mod = {
            'name': module.name,
            'address': module.address,
            'address_hex': module.hex_address(),
            'type': module.dev_type.name,
            'channels': [],
            'status': module.status
        }
        mod_channels = ChannelDescriptor.objects.filter(dev_type__module=module).order_by('ch').all()
        for channel in mod_channels:
            mod['channels'].append(f"{channel.value_type} - {channel.description}")

        devices.append(mod)

    return render(request, 'modulesView.html', {'modules': devices, 'message': message})


def add_module(request):
    ser = None
    module_info = {
        'type_name': None,
        'address': None,
        'address_hex': None,
        'type': None,
        'channels': [],
        'sn': None,
        'fw': None
    }
    message = {
        'title': None,
        'text': None,
        'type': None
    }

    if request.method == 'POST':
        if 'addrForm' in request.POST:
            data = request.POST['module_address']
            module_info['address'] = int(data, 16)
            module_info['address_hex'] = data

            known_modules = Module.objects.filter(address=module_info['address']).first()
            if not known_modules:

                bus = RS485()

                try:
                    dev_dsc = bus.get_descriptor(module_info['address'])
                except SystemExit:
                    message['title'] = "Module Not found"
                    message['text'] = f"Module with address 0x{data} not found on bus"
                    message['type'] = "danger"
                    return render(request, 'addModule.html',
                                  {'module_info': module_info, 'message': message})

                data_dsc = bus.get_data_descriptor(module_info['address'], dev_dsc.n_channels)

                known_type = DevTypeDescriptor.objects.filter(code=dev_dsc.type_id).first()

                if not known_type:  # I don't know this type of module
                    known_type = DevTypeDescriptor(
                        code=dev_dsc.type_id,
                        n_channels=dev_dsc.n_channels

                    )
                    known_type.save()
                    for ch in range(dev_dsc.n_channels):
                        new_channel = ChannelDescriptor(
                            ch=ch,
                            dev_type=known_type,
                            value_type=ValueType.objects.get(code=data_dsc[ch])
                        )
                        new_channel.save()

                module_info['type'] = str(known_type.code)
                module_info['type_name'] = known_type.name
                module_info['sn'] = dev_dsc.sn
                module_info['fw'] = dev_dsc.fw_ver

                mod_channels = ChannelDescriptor.objects.filter(dev_type=known_type).order_by('ch').all()
                for channel in mod_channels:
                    module_info['channels'].append(f"CH{channel.ch}: {channel.value_type}")

            else:
                message['title'] = "Module Exists"
                message['text'] = f"Module with address {data} already exists"
                message['type'] = "danger"

        if 'devForm' in request.POST:
            dev = Module(
                name=request.POST['dev_name'],
                serial_number=int(request.POST['dev_sn']),
                firmware_version=int(request.POST['dev_fw']),
                address=int(request.POST['dev_addr']),
                dev_type=DevTypeDescriptor.objects.filter(code=int(request.POST['dev_type'])).first(),
                status=0,
            )

            dash = grafana_add_dashboard(dev)
            if dash['status'] == "success":
                dev.dashboard_uid = dash['uid']
                dev.dashboard_slug = dash['slug']
                dev.save()
                message['title'] = "Module added"
                message['text'] = f"Module with address {dev.hex_address()} was added successfully"
                message['type'] = "success"

            else:
                message['title'] = "Error"
                message['text'] = "App encountered error when creating new module"
                message['type'] = "danger"

    return render(request, 'addModule.html',
                  {'module_info': module_info, 'message': message})


def limit_edit(request):
    if 'dev' not in request.GET:
        return redirect("/controller/modules")

    module = Module.objects.get(address=int(request.GET['dev']))

    mod = {
        'name': module.name,
        'channels': [],
    }
    mod_channels = ChannelDescriptor.objects.filter(dev_type__module=module).order_by('ch').all()
    for channel in mod_channels:
        limit = ValueLimit.objects.filter(module=module, channel=channel).first()
        ch_limit = {
            'ch_id': channel.id,
            'name': channel.description,
            'min': limit.min_value if limit else None,
            'max': limit.max_value if limit else None,
        }
        mod['channels'].append(ch_limit)

    return redirect('/admin/controller/valuelimit/')


def email_notify(message_type, message):
    notify_users = NotifyContactlist.objects.values_list('email_address', flat=True)
    notify_users = list(notify_users)
    print(notify_users)
    send_mail(
        f'{message_type} message',
        message,
        settings.EMAIL_HOST_USER,
        notify_users,
        fail_silently=False,
    )
