FROM python:3.9

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

WORKDIR /app

COPY ./requirements.txt requirements.txt

RUN apt update
RUN apt -y install cron

RUN pip install -r requirements.txt

RUN update-rc.d cron defaults
RUN update-rc.d cron enable
RUN service cron start